#add debian snapshot with the correct package to sources.list
echo deb http://snapshot.debian.org/archive/debian/20130201T032911Z experimental main contrib non-free >> /etc/apt/sources.list

#update the cache
apt-get -o Acquire::Check-Valid-Until=false update

#install build tools and the correct headers
apt-get -y install build-essential linux-headers-$(uname -r)

#create a symlink, so the tools find the location
ln -s /usr/src/linux-headers-3.7-trunk-amd64/include/generated/uapi/linux/version.h /usr/src/linux-headers-3.7-trunk-amd64/include/linux/version.h

